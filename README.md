# Ansible Elvis

This repository contains an Ansible playbook for setting up my homelab Raspberry Pi.
Elvis is just a name of my Raspberry Pi.

Usually, just running `./play.sh` is enough to set up the Raspberry Pi.