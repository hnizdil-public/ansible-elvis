export CHANGELOG_CONFIG_OVERRIDE=belakos
export APPLE_ID=jan.hnizdil@livesport.eu
export EDITOR=vim
export LC_ALL=en_US.UTF-8
export FIGNORE=.git:DS_Store
export KRB5_CONFIG=~/.krb5.conf
export KRB5CCNAME=~/.krb5.cache
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

# don't open editor for merge commit messages
export GIT_MERGE_AUTOEDIT=no

# Add some colour to LESS/MAN pages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;33m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;42;30m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'

export BASH_SILENCE_DEPRECATION_WARNING=1

function add_bin_paths() {
	local -a paths=(
		"/usr/local/opt/findutils/libexec/gnubin"
		"/usr/local/opt/coreutils/libexec/gnubin"
		"${HOME}/ls/bdsm/bin"
		"${HOME}/bin"
		"/usr/local/sbin"
	)
	for path in "${paths[@]}"; do
		if [[ -d $path ]]; then
			PATH="${path}:${PATH}"
		fi
	done
}

function add_man_paths() {
	local -a paths=(
		"/usr/local/opt/coreutils/libexec/gnuman"
		"/usr/local/opt/findutils/libexec/gnuman"
	)
	for path in "${paths[@]}"; do
		if [[ -d $path ]]; then
			MANPATH="${path}:${MANPATH}"
		fi
	done
}

add_bin_paths
unset add_bin_paths

add_man_paths
unset add_man_paths
export MANPATH

# ls colors
if [[ -f ~/.dircolors ]]; then
	eval $(dircolors ~/.dircolors)
fi

function __vim_ps1() {
	if [[ $VIM_DEPTH ]]; then
		echo "vim${VIM_DEPTH} \$ "
	fi
}

if ! command -v __git_ps1 &>/dev/null; then
	if [[ -f /usr/local/etc/bash_completion.d/git-prompt.sh ]]; then
		source /usr/local/etc/bash_completion.d/git-prompt.sh
	fi
fi

GIT_PS1_SHOWDIRTYSTATE=
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=
GIT_PS1_SHOWUPSTREAM=1
GIT_PS1_SHOWCOLORHINTS=1

if [[ $SSH_CLIENT || $SSH_TTY ]]; then
	PROMPT_COMMAND="__git_ps1 '\$(__vim_ps1)\[\033[1;32m\]\u\[\033[00m\]@\[\033[1;31m\]\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]' ' ' '(%s)'"
else
	PROMPT_COMMAND="__git_ps1 '\$(__vim_ps1)\[\033[01;34m\]\w\[\033[00m\]' ' ' '(%s)'"
fi

if [[ -f ~/.homesick/repos/homeshick/homeshick.sh ]]; then
	. ~/.homesick/repos/homeshick/homeshick.sh
	. ~/.homesick/repos/homeshick/completions/homeshick-completion.bash
fi

if [[ -f ~/.bashrc ]]; then
	source ~/.bashrc
fi
