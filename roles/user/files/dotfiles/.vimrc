set nocompatible

syntax on

scriptencoding utf-8
set encoding=utf-8
set fileencodings=utf-8,latin2

" splits
set splitbelow
set splitright
set noequalalways
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <silent> + :execute "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> - :execute "resize " . (winheight(0) * 2/3)<CR>
nnoremap <silent> <C-M> :execute "resize 999"<CR>

" no trailing space when formatting paragraph
set formatoptions-=w

" default sh file is bash
let g:is_bash=1

" change the mapleader from \ to ,
let mapleader=","

" additional highlighting of *.bak, *.gz, etc groups
let g:netrw_special_syntax=1

" always show statusline
set laststatus=2

set hlsearch
set ignorecase
set smartcase

set pastetoggle=<F12>

set hidden

set wildmenu
set wildmode=list:longest

" no backups and swapfiles
set nobackup
set nowritebackup
set noswapfile

set number

set scrolloff=5

set list
set listchars=nbsp:¬,tab:\ \ ,extends:»,precedes:«,trail:·

set autoindent
set smartindent
set shiftround

set backspace=indent,eol,start

nnoremap H :set cursorline!<CR>
