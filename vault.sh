#!/bin/zsh

ansible-vault $1 --vault-password-file=op-vault-pass.sh $2
