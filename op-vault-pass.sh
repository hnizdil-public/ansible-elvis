#!/bin/zsh

op \
	--account my \
	item get 'Ansible Elvis Vault Password' \
	--vault Private \
	--fields password \
	--reveal
