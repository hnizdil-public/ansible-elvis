#!/bin/zsh

ansible-playbook \
	-i elvis, \
	-e @vault.yml \
	--vault-password-file=op-vault-pass.sh \
	$@ \
	playbook.yml
